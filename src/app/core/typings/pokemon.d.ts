import { Ability } from "./Ability";
import { Species } from "./Species";
import { GameIndex } from "./GameIndex";
import { Move } from "./Move";
import { Sprites } from "./Sprites";
import { Stat } from "./Stat";
import { Type } from "./Type";

export interface Pokemon {
  abilities?:                Ability[];
  base_experience?:          number;
  forms?:                    Species[];
  game_indices?:             GameIndex[];
  height?:                   number;
  held_items?:               any[];
  id?:                       number;
  is_default?:               boolean;
  location_area_encounters?: string;
  moves?:                    Move[];
  name?:                     string;
  order?:                    number;
  species?:                  Species;
  sprites?:                  Sprites;
  stats?:                    Stat[];
  types?:                    Type[];
  weight?:                   number;
}
