import { Result } from './Result';

export interface List {
  count?: number;
  next?: string | null;
  previous?: string | null;
  results?: Result[];
}
