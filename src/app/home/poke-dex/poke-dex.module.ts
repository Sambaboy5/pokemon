import { LoaderService } from './poke-dex.loader.service';
import { PokeService } from './poke-dex.service';
import {
  MatCardModule,
  MatListModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSelectModule,
  MatTooltipModule,
  MatOptionModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
} from '@angular/material';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PokeDexRoutingModule } from './poke-dex-routing.module';
import { PokeDexComponent } from './poke-dex.component';


@NgModule({
  declarations: [PokeDexComponent],
  imports: [
    CommonModule,
    PokeDexRoutingModule,
    CommonModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatPaginatorModule,
    MatTooltipModule,
    CdkTableModule,
    Ng2SearchPipeModule,
    MatIconModule,
    MatInputModule,
  MatProgressSpinnerModule
  ],
  providers: [
    PokeService,
    LoaderService
  ]
})
export class PokeDexModule { }
