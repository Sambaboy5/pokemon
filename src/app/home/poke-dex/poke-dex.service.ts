import { Observable, forkJoin, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { List } from 'src/app/core/typings';
import { LoaderState } from 'src/app/core/typings/Loader';

@Injectable({
  providedIn: 'root'
})
export class PokeService {

  private readonly URL = 'https://pokeapi.co/api/v2/pokemon/';
  private loaderSubject = new Subject<LoaderState>();
  loaderState = this.loaderSubject.asObservable();
  constructor(private http: HttpClient) {}
    show() {
      this.loaderSubject.next(<LoaderState>{show: false});
  }
    hide() {
      this.loaderSubject.next(<LoaderState>{show: true});
  }


  getAllPokemon(): Observable<List> {
    return this.http.get<List>(this.URL);
  }

  getPokemons(urls: string[]): Observable<any> {
    const observables = urls.map((url: string) => this.http.get(url));
    return forkJoin(observables);
  }

  getPager(
    totalItems: number,
    currentPage: number = 1,
    pageSize: number = 10
) {
    const totalPages = Math.ceil(totalItems / pageSize);

    if (currentPage < 1) {
        currentPage = 1;
    } else if (currentPage > totalPages) {
        currentPage = totalPages;
    }

    let startPage: number;
    let endPage: number;
    if (totalPages <= 10) {
        startPage = 1;
        endPage = totalPages;
    } else {
        if (currentPage <= 6) {
            startPage = 1;
            endPage = 10;
        } else if (currentPage + 4 >= totalPages) {
            startPage = totalPages - 9;
            endPage = totalPages;
        } else {
            startPage = currentPage - 1;
            endPage = currentPage + 4;
        }
    }

    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
    const pages = Array.from(Array(endPage + 1 - startPage).keys()).map(
        i => startPage + i
    );
    return {
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages,
    };
  }


}
