import { Component, OnInit, OnDestroy } from '@angular/core';
import { Result, List, Pokemon } from 'src/app/core/typings';
import { takeUntil, switchMap, map } from 'rxjs/operators';
import { PageEvent, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { PokeService } from './poke-dex.service';
import { Subject, Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-poke-dex',
  templateUrl: './poke-dex.component.html',
  styleUrls: ['./poke-dex.component.scss']
})
export class PokeDexComponent implements OnInit, OnDestroy {
  public results: Pokemon[] = [];
  public pagedItems: Pokemon[] = [];
  public term: string;
  private destroy$: Subject<boolean> = new Subject();
  public pageSize = 21;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public pokemon: Pokemon;
  show = false;
  private subscription: Subscription;
  private pageIndex = 0;
  public count = 0;
 public magicObs$: Observable<Pokemon[]>;

  constructor(
    private service: PokeService,
    private router: Router,
  ) {}

  public ngOnInit() {
    this.magicObs$ =  this.getSite(this.pageIndex, this.pageSize);
    this.show = true;
    setTimeout(function() {
        this.show = false;
    }.bind(this), PageEvent.length);
  }

  private getSite(pageIndex?: number, pageSize?: number): Observable<Pokemon[]> {
    return this.service
      .getAllPokemon()
      .pipe(
        map((list: List) => {
          this.count = list.count;
          return list.results;
        }),
        map((results: Result[]) =>
          results.slice(
              pageIndex * pageSize,
              pageIndex * pageSize + pageSize
            ).map((x: Result) => x.url)
        ),
        switchMap((urls: string[]) => this.service.getPokemons(urls)),
        takeUntil(this.destroy$),
      );

  }

  public ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
    this.subscription.unsubscribe();
  }

  public onClickViewDetails(result: Result): void {
    this.router.navigate(['poke', 'pokemon', result.name]);

  }

  public updatePagedItems($event: PageEvent): void {
    if (this.results) {
      this.magicObs$ = this.getSite($event.pageIndex, $event.pageSize);
    }
  }
}
