import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LoaderState } from 'src/app/core/typings/Loader';

@Injectable()

export class LoaderService {
    private loaderSubject = new Subject<LoaderState>();
    loaderState = this.loaderSubject.asObservable();

    constructor() { }

    show() {
        this.loaderSubject.next(<LoaderState>{show: false});
    }

    hide() {
        this.loaderSubject.next(<LoaderState>{show: true});
    }
}
