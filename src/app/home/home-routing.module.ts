import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokeDexComponent } from './poke-dex/poke-dex.component';

const routes: Routes = [
  { path: 'home', redirectTo: 'poke', pathMatch: 'full' },
  { path: 'poke', component: PokeDexComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
